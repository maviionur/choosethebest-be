#!/usr/bin/env bash

if [ -z "$TAG" ]
then
    echo "TAG not defined. Define TAG in the environment:"
    read -r tag
    export TAG=$tag
fi

if [ -z "$TAG" ]
then
  echo "TAG variable is required"
  exit 1
fi

cd ansible || exit 1
ansible-playbook deployment.yml -i hosts "$@"
