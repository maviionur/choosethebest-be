#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset

if [ "$APP_ENV" = 'production' ];
then
  exec celery -A config.celery_app worker -l INFO \
        -n "$1@%h" \
        -Q "$2" \
        --pool=prefork;
else
  # follow files in development
  watchfiles --filter python "celery -A config.celery_app worker -l INFO \
        -n ""$1"@%h" \
        -Q ""$2"" \
        --pool=prefork";
fi;