#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "APP_ENV is $APP_ENV"

# If env is production, dont run it with watchfiles
if [ "$APP_ENV" = 'production' ]; then
  exec celery -A config.celery_app beat --loglevel=info
else
  watchfiles --filter python 'celery -A config.celery_app beat --loglevel=info'
fi
