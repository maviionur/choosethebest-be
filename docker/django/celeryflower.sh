#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

celery -A config.celery_app flower --port=5555
