"""Gunicorn configuration.

See:https://docs.gunicorn.org/en/stable/configure.html#configuration-file
See:https://docs.gunicorn.org/en/stable/settings.html
"""

import multiprocessing

bind = "0.0.0.0:8000"
# Concerning `workers` setting see:
# https://github.com/wemake-services/wemake-django-template/issues/1022
workers = multiprocessing.cpu_count() * 2 + 1
worker_connections = 1000
threads = 4

max_requests = 1200
max_requests_jitter = 400
keepalive = 5
timeout = 120
graceful_timeout = 120

chdir = "/app"
worker_tmp_dir = "/dev/shm"  # noqa: S108  # nosec

loglevel = "info"
accesslog = "-"
errorlog = "-"
log_file = "-"
