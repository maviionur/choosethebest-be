#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# We are using `gunicorn` for production, see:
# http://docs.gunicorn.org/en/stable/configure.html

# Check that $DJANGO_ENV is set to "production",
# fail otherwise, since it may break things:
echo "APP_ENV is $APP_ENV"
if [ "$APP_ENV" != 'production' ]; then
  echo 'Error: APP_ENV is not set to "production".'
  echo 'Application will not start.'
  exit 1
fi

export APP_ENV

# Run python specific scripts:
# Running migrations in startup script might not be the best option
python manage.py migrate --noinput
python manage.py collectstatic --noinput

# Start gunicorn:
# Docs: http://docs.gunicorn.org/en/stable/settings.html

/usr/local/bin/gunicorn \
  --config python:docker.django.gunicorn_config \
  config.wsgi
