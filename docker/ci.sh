#!/usr/bin/env bash

set -o nounset

message_error()
{
    printf "\e[31mERROR: %s\e[0m \n" "$1"
}

message_warning()
{
    printf "\e[33mWARNING: %s\e[0m \n" "$1"
}
# shellcheck disable=SC2317
message_success()
{
    printf "\e[32mSUCCESS: %s\e[0m \n" "$1"
}

exit_code=0
run_command() {
    message_warning "Running: $*"
    if ! "$@"; then
        message_error "Failed to run: $*"
        exit_code=1
    fi
}


# Initializing global variables and functions:
: "${APP_ENV:=development}"


# Fail CI if `APP_ENV` is not set to `development`:
if [ "$APP_ENV" != 'development' ]; then
  echo 'APP_ENV is not set to development. Running tests is not safe.'
  exit 1
fi


pyclean () {
  # Cleaning cache:
  find . \
    | grep -E '(__pycache__|\.hypothesis|\.perm|\.cache|\.static|\.py[cod]$)' \
    | xargs rm -rf \
  || true
}

run_ci () {
  message_warning '[ci started]'
  set -x  # we want to print commands during the CI process.

  # Testing filesystem and permissions:
  touch .perm && rm -f .perm
  touch '/var/www/django/media/.perm' && rm -f '/var/www/django/media/.perm'
  touch '/var/www/django/static/.perm' && rm -f '/var/www/django/static/.perm'

  # Running linting for all python files in the project:
  run_command make ruff

  # Checking `pyproject.toml` file contents:
  run_command poetry check

  # Checking dependencies status:
  run_command pip check


  # Running tests:
  # dead-fixtures disabled because of: https://github.com/jllorencetti/pytest-deadfixtures/issues/28
  # run_command pytest --dead-fixtures
  run_command make pytest

  message_warning '[ci finished]'
}

# Remove any cache before the script:
pyclean

# Clean everything up:
trap pyclean EXIT INT TERM

# Run the CI process:
run_ci

# Exit with the exit code:
# Gitlab ci looks for exit code 0 to know if the script succeeded.
exit $exit_code
