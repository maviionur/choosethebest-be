#!/bin/bash


# no SSL certificates on first run
# so we wait certbot to create new SSL certificates.
until [ -f /etc/letsencrypt/live/"${DOMAIN_NAME}"/fullchain.pem ]
do
    echo "SSL certificates not found. Waiting certbot."
    sleep 5
done

while true; do
    sleep 12h & wait ${!};
    nginx -s reload;
done & \
bash docker-entrypoint.sh nginx -g "daemon off;"
