#!/bin/sh

echo "Creating certificates"
while :; do
    certbot certonly \
        --non-interactive \
        --dns-cloudflare \
        --dns-cloudflare-credentials /cloudflare.ini \
        -w /var/www/certbot \
        -m 'fatihtufekci1997@gmail.com' \
        -d "$DOMAIN_NAME" \
        --rsa-key-size '2048' \
        --agree-tos \
        -n

    # sleep 12h
    echo "Sleeping..."
    sleep 12h & wait ${!};
done
