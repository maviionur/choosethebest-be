import os
from functools import partial
from typing import Any

from celery import Celery, Task
from django.db import transaction

# set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")


class CtbTask(Task):
    """Customize Celery Task for Choose the Best."""

    def delay_on_commit(self, *args: Any, **kwargs: Any) -> None:
        """Call task after transaction on commit."""
        return transaction.on_commit(partial(self.delay, *args, **kwargs))

    def apply_async_on_commit(self, *args: Any, **kwargs: Any) -> None:
        """Call task after transaction on commit."""
        return transaction.on_commit(partial(self.apply_async, *args, **kwargs))


celery_app = Celery("ctb", task_cls=CtbTask)
celery_app.config_from_object("django.conf:settings", namespace="CELERY")
celery_app.autodiscover_tasks()
