from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView


def trigger_error(request):
    division_by_zero = 1 / 0

urlpatterns = [
    path(f"{settings.ADMIN_URL}/", admin.site.urls),
    # Text and xml static files:
    path(
        "robots.txt",
        TemplateView.as_view(
            template_name="txt/robots.txt",
            content_type="text/plain",
        ),
    ),
    # Sentry
    path('sentry-debug/', trigger_error),
]

# Rest framework endpoints
urlpatterns += [
    path("", include("config.frontend_router")),
]


if settings.DEBUG:  # pragma: no cover
    from django.conf.urls.static import static

    urlpatterns = [
        # to develop bad request pages.
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
        *urlpatterns,
        # Serving media files in development only:
        *static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT),
        # Static file serving when using Gunicorn + Uvicorn for local web socket development
        *staticfiles_urlpatterns(),
    ]



