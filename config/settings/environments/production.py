"""
This file contains all the settings used in production.

This file is required and if development.py is present these
values are overridden.
"""

import sentry_sdk

from config.settings.components import env
from config.settings.components.common import MIDDLEWARE

# Production flags:
# https://docs.djangoproject.com/en/3.2/howto/deployment/

DEBUG = False

DOMAIN_NAME = env("DOMAIN_NAME")
# It used to create absolute urls:
USE_SSL = env("DJANGO_USE_SSL", cast=bool, default=True)

ALLOWED_HOSTS = [
    DOMAIN_NAME,
    # We need this value for `healthcheck` to work:
    "localhost",
    # We need this value for nginx access:
    "app",
    # AllowCIDRMiddleware uses it.
    "*",
]

ALLOWED_CIDR_NETS = ["10.30.0.0/19", "10.40.0.0/19"]

MIDDLEWARE += (
    # Allow AWS CIDR:
    # https://github.com/mozmeao/django-allow-cidr
    "allow_cidr.middleware.AllowCIDRMiddleware",
)

# Application definition:
SECRET_KEY = env("DJANGO_SECRET_KEY")

# ADMIN
# Django Admin URL regex.
ADMIN_URL = env("DJANGO_ADMIN_URL")

# Staticfiles
# https://docs.djangoproject.com/en/3.2/ref/contrib/staticfiles/

# Media files
# https://docs.djangoproject.com/en/3.2/topics/files/


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

_PASS = "django.contrib.auth.password_validation"  # noqa: S105
AUTH_PASSWORD_VALIDATORS = [
    {"NAME": f"{_PASS}.UserAttributeSimilarityValidator"},
    {"NAME": f"{_PASS}.MinimumLengthValidator"},
    {"NAME": f"{_PASS}.CommonPasswordValidator"},
    {"NAME": f"{_PASS}.NumericPasswordValidator"},
]

# Security
# https://docs.djangoproject.com/en/3.2/topics/security/

SECURE_HSTS_SECONDS = 31536000  # the same as Caddy has
SECURE_HSTS_INCLUDE_SUBDOMAINS = env(
    "DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS", cast=bool, default=True
)
SECURE_HSTS_PRELOAD = env("DJANGO_SECURE_HSTS_PRELOAD", cast=bool, default=True)

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
SECURE_SSL_REDIRECT = env("DJANGO_SECURE_SSL_REDIRECT", cast=bool, default=False)
SECURE_REDIRECT_EXEMPT: list[str] = [
    # This is required for healthcheck to work:
    # "^health/",
]

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# EMAIL
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL",
    default="UFUFU <noreply@ufufu.com>",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL", default=DEFAULT_FROM_EMAIL)


# django-cors-headers - https://github.com/adamchainz/django-cors-headers#setup
CORS_ALLOWED_ORIGINS = env(
    "DJANGO_CORS_ALLOWED_ORIGINS",
    default=[],
    cast=list,
)
CSRF_TRUSTED_ORIGINS = CORS_ALLOWED_ORIGINS

# Reverse proxy like nginx, cloudflare
BEHIND_REVERSE_PROXY = True
REVERSE_PROXY_HEADER = "X-Forwarded-For"

# Sentry sdk
sentry_sdk.init(
    dsn=env("SENTRY_DSN", default=""),
    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    traces_sample_rate=1.0,
    # Set profiles_sample_rate to 1.0 to profile 100%
    # of sampled transactions.
    # We recommend adjusting this value in production.
    profiles_sample_rate=1.0,
)
