"""
This file contains all the settings that defines the development server.

SECURITY WARNING: don't run with debug turned on in production!
"""

import os
import socket

from config.settings.components import BASE_DIR, env
from config.settings.components.common import DATABASES, INSTALLED_APPS

DEBUG = True

ALLOWED_HOSTS = [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
    "[::1]",
]

#  Installed apps for development only:

INSTALLED_APPS += (
    # django-extension:
    # https://github.com/django-extensions/django-extensions
    "django_extensions",
)


# https://django-debug-toolbar.readthedocs.io/en/stable/installation.html#configure-internal-ips
try:  # This might fail on some OS
    INTERNAL_IPS = [
        "{ip}.1".format(ip=ip[: ip.rfind(".")])
        for ip in socket.gethostbyname_ex(socket.gethostname())[2]
    ]
except OSError:  # pragma: no cover
    INTERNAL_IPS = []
INTERNAL_IPS += ["127.0.0.1", "10.0.2.2"]



# EMAIL
# https://docs.djangoproject.com/en/dev/ref/settings/#email-host
EMAIL_HOST = env("EMAIL_HOST", default="mailhog")
# https://docs.djangoproject.com/en/dev/ref/settings/#email-port
EMAIL_PORT = 1025
EMAIL_USE_TLS = False


# Django Extension
SHELL_PLUS = "ipython"

SHELL_PLUS_PRINT_SQL = True

NOTEBOOK_ARGUMENTS = [
    "--ip",
    "0.0.0.0",  # noqa: S104 # nosec
    "--port",
    "8888",
    "--allow-root",
    "--no-browser",
    "--notebook-dir",
    str(BASE_DIR.joinpath("notebooks")),
]

IPYTHON_ARGUMENTS = [
    "--ext",
    "django_extensions.management.notebook_extension",
    "--debug",
]

IPYTHON_KERNEL_DISPLAY_NAME = "Django Shell-Plus"

# only use in development
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"


# Disable persistent DB connections
# https://docs.djangoproject.com/en/3.2/ref/databases/#caveats
DATABASES["default"]["CONN_MAX_AGE"] = 0
