from config.settings.components import env

# RABBITMQ_SERVICE
RABBITMQ_SERVICE_HOST = env("RABBITMQ_SERVICE_HOST", default="broker")
RABBITMQ_SERVICE_PORT = env("RABBITMQ_SERVICE_PORT", cast=int, default=5672)
RABBITMQ_SERVICE_SSL = env("RABBITMQ_SERVICE_SSL", cast=bool, default=False)
