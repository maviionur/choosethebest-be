from kombu import Exchange, Queue

from config.settings.components import env
from config.settings.components.common import TIME_ZONE, USE_TZ

# Backend Celery configuration
CELERY_BROKER_URL = env("BROKER_URL")
CELERY_CREATE_MISSING_QUEUES = True

if USE_TZ:
    # https://docs.celeryq.dev/en/stable/userguide/configuration.html#std:setting-timezone
    CELERY_TIMEZONE = TIME_ZONE

CELERY_TASK_TRACK_STARTED = True
CELERY_ACCEPT_CONTENT = ["json", "pickle"]
CELERY_TASK_SERIALIZER = "json"
CELERY_RESULT_SERIALIZER = "json"
# https://docs.celeryq.dev/en/stable/userguide/configuration.html#task-time-limit
CELERY_TASK_TIME_LIMIT = 5 * 60
CELERY_TASK_SOFT_TIME_LIMIT = 60

# https://docs.celeryq.dev/en/stable/userguide/configuration.html#beat-scheduler
CELERY_BEAT_SCHEDULER = "django_celery_beat.schedulers:DatabaseScheduler"

# For celery 5.x settings
# CELERY_DEFAULT_QUEUE, CELERY_QUEUES, CELERY_ROUTES should be named CELERY_TASK_DEFAULT_QUEUE,
# CELERY_TASK_QUEUESand CELERY_TASK_ROUTES instead. These are settings that I've tested,
# but my guess is the same rule applies for exchange and routing key as well.
CELERY_TASK_DEFAULT_QUEUE = "default"
# TODO: update all queue
CELERY_TASK_QUEUES = (
    Queue("default", Exchange("default"), routing_key="default"),
    Queue("email", Exchange("email"), routing_key="email"),
)

CELERY_DEFAULT_EXCHANGE = "tasks"
CELERY_DEFAULT_EXCHANGE_TYPE = "direct"
CELERY_DEFAULT_ROUTING_KEY = "task.default"


# Periodic tasks
CELERY_BEAT_SCHEDULE = {
}
