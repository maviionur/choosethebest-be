

REST_FRAMEWORK = {
    "DEFAULT_PAGINATION_CLASS": "apps.quiz.pagination.DefaultPagination",
    "PAGE_SIZE": 100,
    "DEFAULT_FILTER_BACKENDS": ["django_filters.rest_framework.DjangoFilterBackend"]
}
