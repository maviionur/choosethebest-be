from pathlib import Path

import environ

BASE_DIR = Path(__file__).parent.parent.parent.parent


env = environ.Env(DEBUG=(bool, False))

environ.Env.read_env(BASE_DIR.joinpath("envs", ".env"))
