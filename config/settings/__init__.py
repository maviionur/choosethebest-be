"""
This is a django-split-settings main file.

For more information read this:
https://github.com/sobolevn/django-split-settings
https://sobolevn.me/2017/04/managing-djangos-settings

To change settings file:
`DJANGO_ENV=production python manage.py runserver`
"""

from os import environ

from split_settings.tools import include


# Managing environment via `APP_ENV` variable:
environ.setdefault("APP_ENV", "development")
_ENV = environ["APP_ENV"]

_base_settings = (
    "components/common.py",
    "components/celery.py",
    "components/rabbitmq.py",
    "components/rest_framework.py",
    # Select the right env:
    f"environments/{_ENV}.py",
)

# Include settings:
include(*_base_settings)
