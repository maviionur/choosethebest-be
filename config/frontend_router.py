from rest_framework.routers import DefaultRouter, SimpleRouter
from django.conf import settings
from apps.quiz.views import AttachmentViewSet, CategoryViewSet, QuizViewSet, QuizRateViewSet


if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("categories", CategoryViewSet, basename="categories")
router.register("quizes", QuizViewSet, basename="quizes")
router.register("quiz-rates", QuizRateViewSet, basename="quiz-rates")
router.register("attachments", AttachmentViewSet, basename="attachments")

api_view_urls = [
    # Path example
    # path("test/stats", TestAPIView.as_view(), name="test-stats"),

    # Repath example
    # re_path(
    #     "^test/pages/(?P<path>.*)$",
    #     TestAPIView.as_view(),
    #     name="test-dnses",
    # ),
]


urlpatterns = [*api_view_urls, *router.urls]

