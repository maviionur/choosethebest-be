#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


CERT_PATH=${CERT_PATH-docker/nginx/certs}
# Diffie-Hellman key exchange is a protocol providing the pretty cool property that,
# even if some attackers get their hands on your server's private key, it will be exponentially hard for them to
# decipher the communication between the server and its clients. However, the default key size in OpenSSL is 1024 bits,
#  which seems breakable with the computing power of a nation-state. So, you let's generate some better parameters.
if [[ ! -f $CERT_PATH/dhparams4096.pem ]]; then
    echo "Generating 4096-bit Diffie-Hellman parameters..."
    openssl dhparam -out docker/nginx/certs/dhparams4096.pem 4096
fi
