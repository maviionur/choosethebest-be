#!/usr/bin/env bash

# Exit in case of error
set -o errexit
set -o pipefail
set -o nounset

# build for production
APP_UID=${APP_UID?Variable not set} \
APP_GID=${APP_GID?Variable not set} \
TAG=${TAG?Variable not set} \
WORKDIR=${WORKDIR?Variable not set} \
DOCKER_BUILDKIT=1 \
docker compose -f docker-compose.yml -f docker/docker-compose.prod.yml build
