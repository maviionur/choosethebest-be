#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


if [[ ! -f docker/nginx/certs/selfsigned.key ]]
then
  printf "Certificate file not found. Creating...\n"

  # create self signed ssl for default nginx server.
  openssl req -x509 -nodes -days 3650 -newkey rsa:4096 \
    -subj "/C=US/ST=/L=/O=/CN=''" \
    -keyout docker/nginx/certs/selfsigned.key \
    -out docker/nginx/certs/selfsigned.crt
fi
