#!/usr/bin/env bash

# Exit in case of error
set -o errexit
set -o pipefail
set -o nounset

# create docker stack using env variables
STACK_NAME=${STACK_NAME?Variable not set} \
TAG=${TAG?Variable not set} \
WORKDIR=${WORKDIR?Variable dnot set} \
# deploy docker stack with swarm mode
docker stack deploy \
    -c docker-compose.yml -c docker/docker-compose.prod.yml \
    --with-registry-auth "${STACK_NAME?Variable not set}"
