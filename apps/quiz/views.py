from django.db import models, transaction
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.filters import SearchFilter
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from apps.quiz.filters import QuizFilter
from apps.quiz.models import Attachment, Category, Quiz, QuizRate
from apps.quiz.serializers import (AttachmentScoreSerializer, AttachmentSerializer,
                              AttachmentTitleSerializer, CategorySerializer,
                              QuizRateSerializer, QuizSerializer)
from apps.quiz.services import quiz_create


class QuizViewSet(ModelViewSet):
     
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["category__id"]
    search_fields = ["title"]
    queryset = Quiz.objects.all()
    serializer_class = QuizSerializer
    filterset_class = QuizFilter
    http_method_names = ["post", "get", "delete"]
    lookup_field = "id"

    def get_queryset(self) -> models.QuerySet[Quiz]:
        """Get queryset."""
        queryset = self.queryset.filter(is_visible=True).annotate(average_rate=models.Avg("quiz_rate__rate_score")).select_related("category").prefetch_related("attachments")
        if self.action == "top_rated":
            queryset = queryset.order_by(models.F('average_rate').desc(nulls_last=True))
        return queryset.order_by(models.F('average_rate').desc(nulls_last=True))
    
    def retrieve(self, request, *args, **kwargs):
        data = super().retrieve(request, *args, **kwargs).data
        attachments = data.get("attachments", [])
        attachments = sorted(attachments, key=lambda item: item['score'], reverse=True)
        data["attachments"] = attachments
        return Response(data)

    def create(self, request: Request, *args: object, **kwargs: object) -> Response:
        """Create quiz endpoint"""
        serializer = QuizSerializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)
        quiz = quiz_create(**serializer.validated_data)
        serializer.instance = quiz
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    @action(detail=False, methods=["get"], url_name="top-rated", url_path="top-rated")
    def top_rated(self, request: Request, *args: object, **kwargs: object) -> Response:
        """Get top rated quizes."""
        return self.list(request, args, kwargs)



class QuizRateViewSet(ModelViewSet):

    queryset = QuizRate.objects.all()
    serializer_class = QuizRateSerializer
    http_method_names = ["get", "post"]
    lookup_field = "id"


class CategoryViewSet(ModelViewSet):

    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filter_backends = [DjangoFilterBackend, SearchFilter]
    filterset_fields = ["name"]
    search_fields = ["name"]
    http_method_names = ["post", "get"]
    lookup_field = "id"



class AttachmentViewSet(ModelViewSet):

    queryset = Attachment.objects.all().order_by("-created_at")
    serializer_class = AttachmentSerializer
    http_method_names = ["post", "get", "put", "patch", "delete"]
    lookup_field = "id"

    def get_queryset(self) -> models.QuerySet[Quiz]:
        """Get queryset."""
        queryset = self.queryset.annotate(average_rate=models.Avg("quiz__quiz_rate__rate_score")).select_related("quiz").prefetch_related("quiz__quiz_rates")
        if self.action == "top_rated":
            queryset = queryset.order_by(models.F('average_rate').desc(nulls_last=True))
        return queryset.order_by(models.F('average_rate').desc(nulls_last=True))
    
    @transaction.atomic
    @action(detail=True, methods=["put"], url_name="set-score", url_path="set-score", serializer_class=AttachmentScoreSerializer)
    def set_score(self, request: Request, *args: object, **kwargs: object) -> Response:
        """Set attachment score."""
        attachment = self.get_object()
        attachment = Attachment.objects.select_for_update().get(pk=attachment.pk)
        attachment.score += 1
        attachment.save(update_fields=["score"])
        return self.list(request, args, kwargs)
    
    @transaction.atomic
    @action(detail=False, methods=["patch"], url_name="update-titles", url_path="update-titles", serializer_class=AttachmentTitleSerializer)
    def update_titles(self, request: Request, *args: object, **kwargs: object) -> Response:
        """Update attachment titles."""
        serializer = AttachmentTitleSerializer(data=request.data, context=self.get_serializer_context())
        serializer.is_valid(raise_exception=True)

        ids = serializer.validated_data["ids"]
        titles = serializer.validated_data["titles"]
        id_title_map = {ids[index]: titles[index] for index in range(len(ids))}

        attachments = Attachment.objects.filter(id__in=ids)

        if len(attachments) != len(ids):
            raise ValidationError("Same length error!!")

        update_objects = []
        for attachment in attachments:
            attachment.title = id_title_map[attachment.id]
            update_objects.append(attachment)

        Attachment.objects.bulk_update(update_objects, fields=["title"])

        return Response(status=status.HTTP_200_OK)
