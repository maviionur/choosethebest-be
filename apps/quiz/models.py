from django.db import models
from django.utils import timezone


# Create your models here.
class BaseModel(models.Model):

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now_add=True)
    

    class Meta:
        abstract = True


class Category(BaseModel):

    name = models.CharField(max_length=255)

    def __str__(self) -> str:
        return self.name



class Quiz(BaseModel):

    title = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True)
    image = models.ImageField(upload_to="background_images", null=True)
    is_image =  models.BooleanField(default=True)
    is_visible = models.BooleanField(default=True)

    def __str__(self) -> str:
        return self.title



class QuizRate(BaseModel):

    rate_score = models.PositiveIntegerField(default=0)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="quiz_rates", related_query_name="quiz_rate",)

    def __str__(self) -> str:
        return f"risk_score: {self.rate_score}"



class Attachment(BaseModel):

    title = models.CharField(max_length=255)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name="attachments", related_query_name="attachment", null=True)
    url = models.URLField(null=True)
    image = models.ImageField(upload_to="images/", null=True)
    score = models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return self.title