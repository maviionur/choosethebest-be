from django.contrib import admin

from apps.quiz.models import Attachment, Category, Quiz, QuizRate

# Register your models here.
admin.site.register(Category)
admin.site.register(Quiz)
admin.site.register(Attachment)
admin.site.register(QuizRate)
