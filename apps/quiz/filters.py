from django_filters.filters import NumberFilter, OrderingFilter
from django_filters.filterset import FilterSet

from apps.quiz.models import Quiz


class QuizFilter(FilterSet):

    category__id = NumberFilter(field_name="category__id")

    ordering = OrderingFilter(
        fields=(
            ("created_at", "created_at"),
        ),
    )

    class Meta:
        model = Quiz
        fields = (
            "title",
            "category__id",
        )
