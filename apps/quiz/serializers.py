from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.quiz.models import Attachment, Category, Quiz, QuizRate


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ["id", "name", "created_at", "updated_at"]

class AttachmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attachment
        fields = ["id", "title", "url", "image", "score", "created_at", "updated_at"]
        read_only_fields = ["id", "created_at", "updated_at"]


class AttachmentScoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attachment
        fields = ["id"]
        read_only_fields = ["id"]

class AttachmentTitleSerializer(serializers.Serializer):

    ids = serializers.ListField(child=serializers.IntegerField(), allow_empty=False)
    titles = serializers.ListField(child=serializers.CharField(), allow_empty=False)

    class Meta:
        fields = ["ids", "titles"]

    def validate(self, attrs):
        if len(attrs["ids"]) != len(attrs["titles"]):
            raise ValidationError("Same length error!!")
    
        return attrs

    

class QuizSerializer(serializers.ModelSerializer):

    attachments = AttachmentSerializer(many=True, read_only=True)
    category = CategorySerializer(read_only=True)
    category_id = serializers.PrimaryKeyRelatedField(queryset=Category.objects.all(), write_only=True, source="category",)
    average_rate = serializers.FloatField(read_only=True)
    attachment_ids = serializers.PrimaryKeyRelatedField(queryset=Attachment.objects.filter(quiz_id=None), source="attachments", write_only=True, required=True, many=True)

    class Meta:
        model = Quiz
        fields = ["id", "title", "attachments", "attachment_ids","image", "category", "category_id", "created_at", "is_visible", "average_rate", "is_image"]
        read_only_fields = ["id", "created_at", "attachments", "category", "average_rate"]



class QuizRateSerializer(serializers.ModelSerializer):

    quiz = QuizSerializer(read_only=True)
    quiz_id = serializers.PrimaryKeyRelatedField(queryset=Quiz.objects.all(), write_only=True, source="quiz",)
    rate_score = serializers.IntegerField(min_value=0, max_value=5)
    class Meta:
        model = QuizRate
        fields = ["id", "created_at", "updated_at", "quiz", "quiz_id", "rate_score"]
        read_only_fields = ["id", "created_at", "updated_at"]
