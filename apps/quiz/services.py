from django.db import transaction
from django.utils import timezone
from PIL import Image

from apps.quiz.models import Attachment, Category, Quiz


@transaction.atomic
def quiz_create(
    *,
    title: str,
    category: Category,
    is_visible: bool,
    is_image: bool,
    image: Image,
    attachments: list[Attachment],
) -> Quiz:
    """Create quiz service."""
    quiz = Quiz.objects.create(title=title, category=category, is_visible=is_visible, image=image)
    Attachment.objects.filter(id__in=[attachment.id for attachment in attachments]).update(quiz=quiz, updated_at=timezone.now())
    return quiz
