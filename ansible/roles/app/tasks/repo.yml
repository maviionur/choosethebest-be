---

- name: Get user home directory
  ansible.builtin.shell: >
    set -o pipefail && getent passwd {{ ansible_user }} | awk -F: '{ print $6 }'
  args:
    executable: /usr/bin/bash
  changed_when: false
  register: app_user_home_output
  tags:
    - ansible-home
    - always

- name: Set user_home variable
  ansible.builtin.set_fact:
    app_user_home: "{{ app_user_home_output.stdout }}"
  tags:
    - ansible-home
    - always

- name: Create .ssh directory
  ansible.builtin.file:
    path: "{{ app_user_home }}/.ssh"
    state: directory
    mode: 0755  # it's default folder permission.

- name: Copy ssh keys to .ssh directory
  ansible.builtin.copy:
    src: "{{ item.src }}"
    dest: "{{ app_user_home }}/.ssh/{{ item.dest }}"
    owner: "{{ ansible_user }}"
    group: "{{ ansible_user }}"
    mode: "{{ item.mode }}"
  with_items:
    - { src: git_id_rsa.priv, dest: id_rsa, mode: "600" }
    - { src: git_id_rsa.pub, dest: id_rsa.pub, mode: "644" }
  tags:
    - ssh-copy-keys

- name: Git safe repository directory  # noqa: command-instead-of-module
  ansible.builtin.shell: >
    git config --global --add safe.directory {{ project_code_path }}
  register: app_my_output
  changed_when: app_my_output.rc != 0
  tags:
    - clone-repo

- name: Clone repository
  ansible.builtin.git:
    repo: "{{ git_repo_url }}"
    dest: "{{ project_code_path }}"
    clone: yes
    update: yes
    accept_hostkey: yes
    key_file: "{{ app_user_home }}/.ssh/id_rsa"
    version: "{{ git_repo_branch }}"
    force: true
  tags:
    - clone-repo

- name: Change ownership for code files
  ansible.builtin.file:
    path: "{{ project_code_path }}"  # this should be as same as `dest` above
    owner: "{{ project_user }}"
    group: "{{ project_user }}"
    state: directory
    recurse: yes
  tags:
    - clone-repo
