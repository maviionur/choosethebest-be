SHELL:=/usr/bin/env bash

install-pre-commit:
	@pre-commit install
	@pre-commit install --hook-type commit-msg

black-check:
	@poetry run black . --check 	# check black version

black-fix: ## run black on all python files
	@poetry run black --version 	# check black version
	@poetry run black .	# run black

ruff:	## run ruff on all python files
	@poetry run ruff check . --config=pyproject.toml

django-check: ## run django check command
	# Run checks to be sure we follow all django's best practices:
	@poetry run python manage.py check --fail-level WARNING
	# Run checks to be sure settings are correct (production flag is required):
	APP_ENV=production poetry run python manage.py check --deploy --fail-level WARNING
	# Check that staticfiles app is working fine:
	@poetry run python manage.py collectstatic --no-input --dry-run
	# Run checks to be sure we follow all django's best practices:
	@poetry run python manage.py makemigrations --dry-run --check

gunicorn-check: ## Check production settings for gunicorn:
	@poetry run gunicorn --check-config --config python:docker.django.gunicorn_config config.wsgi

pytest:	## run pytest
	@poetry run pytest

pytest-dead-fixtures: ## run pytest with dead-fixtures
	@poetry run pytest --dead-fixtures

package: ## check security vulnerabilities
	@poetry check
	@poetry run pip check
	@poetry run safety check --full-report

black: black-fix ## run all black commands

lint: black ruff ## run all lint commands

test: lint package pytest ## run all test commands

clean: # clean all files and folders
	@find . -name '*.pyc' -exec rm -rf {} \;
	@find . -name 'pycache' -exec rm -rf {} \;
	@find . -name 'Thumbs.db' -exec rm -rf {} \;
	@find . -name '*~' -exec rm -rf {} \;
	@find . -name '__pycache__/' -exec rm -rf {} \;
	@find . -name '__pypackages__/' -exec rm -rf {} \;
	@find . -name '.DS_Store' -exec rm -rf {} \;
	rm -rf .cache
	rm -rf .pytest_cache
	rm -rf .ruff_cache
	rm -rf .tox
	rm -rf .hypothesis
	rm -rf .eggs
	rm -rf htmlcov

.PHONY: install-pre-commit
				black-check
				black-fix
				ruff
				pytest
				package
				django-check
				gunicorn-check
